## Setup do ambiente

Para executar o projeto é necessário ter :

1. O [compilador de swift 4.*](https://swift.org/download/) instalado.
2. O [framework vapor](https://docs.vapor.codes/2.0/) instalado

## Alterar api key

Vá em Config->app.json e altere a api key.

## Executar

Existem duas formas de executar o projeto

1. Abra o .xcodeproj do projeto e execute o Run. Caso não existe o arquivo com extensão .xcodeproj, utilize o comando vapor xcode ou swift package generate-xcodeproj 
2. Execute o comando swift build na pasta do projeto. Após a compilação será criado o executável do projeto. Então use o seguinte comando : .build/x86_64-apple-macosx10.10/debug/Run 
