import HTTP
import Vapor

public enum enumPaymentMethod : String{
    case boleto
    case credit_card
}

final class Routes: RouteCollection {
    let view: ViewRenderer
    let drop : Droplet
    
    init(_ view: ViewRenderer, droplet : Droplet) {
        self.view = view
        self.drop = droplet
    }

    func build(_ builder: RouteBuilder) throws {
        
        builder.get { req in
            return try self.view.make("checkout")
        }
        
        builder.post("buy", handler: { req in
            
            guard let dataEncoded = req.formURLEncoded else{
                throw Abort.badRequest
            }
            
            guard let object = dataEncoded.object else{
                throw Abort.serverError
            }
            
            guard let pagarme = object["pagarme"] else{
                throw Abort.serverError
            }
            
            guard let amount = try? pagarme.get("amount") as String else{
                throw Abort.serverError
            }
            
            guard let paymentMethod = try? pagarme.get("payment_method") as String else{
                throw Abort.serverError
            }
            
            guard let config = self.drop.config["app", "api_key"] else{
                throw Abort.serverError
            }
            
            guard let apiKey = config.string else{
                throw Abort.serverError
            }
            
            let req = Request(method: .post, uri: "https://api.pagar.me/1/transactions")
            
            var json : JSON = JSON()

            try json.set("api_key", apiKey)
            try json.set("amount", amount)
            try json.set("payment_method", paymentMethod)
            
            if paymentMethod.equals(caseInsensitive: enumPaymentMethod.credit_card.rawValue){
                 try json.set("card_hash",try pagarme.get("card_hash") as String)
            }

            req.json = json
            
            let response = try self.drop.client.respond(to: req)

            if response.status == .ok{
                let resp = try Response(status: .ok, json: JSON(arrayLiteral: ["message" : "transaction created"]))
                return resp
            }else{
                let resp = Response(status: .badRequest)
                
                return resp
            }
        })
    }
}
